<?php
$servername = "localhost";
$username = "alumno";
$password = "alumno";
$dbname = "mysql_db";

// Crea la conexión
$conn = mysqli_connect($servername, $username, $password, $dbname);
// Check connection
if (!$conn) {
  die("Connection failed: " . mysqli_connect_error());
}

// Crea la tabla productos con tipo de dato corregido para precio
$sql = "CREATE TABLE productos (
id INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
nombre_prod VARCHAR(30) NOT NULL,
precio INT NOT NULL
)";


if (mysqli_query($conn, $sql)) {
  echo "Table usuarios created successfully";
} else {
  echo "Error creating table: " . mysqli_error($conn);
}

mysqli_close($conn);
?>