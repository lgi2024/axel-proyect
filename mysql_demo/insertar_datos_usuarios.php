<?php
$servername = "localhost";
$username = "alumno";
$password = "alumno";
$dbname = "mysql_db";

// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);
// Check connection
if ($conn->connect_error) {
  die("Connection failed: " . $conn->connect_error);
}

// Query to insert data into usuarios table
$sql = "INSERT INTO usuarios (nombre, `contraseña`)
        VALUES ('Axel', 'Doe'),
               ('Juan', 'Doe')";

if ($conn->query($sql) === TRUE) {
  echo "New record created successfully";
} else {
  echo "Error: " . $sql . "<br>" . $conn->error;
}

$conn->close();
?>
