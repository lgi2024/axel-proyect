<?php
$servername = "localhost";
$username = "alumno";
$password = "alumno";
$dbname = "mysql_db";

// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);
// Check connection
if ($conn->connect_error) {
  die("Connection failed: " . $conn->connect_error);
}

$sql = "SELECT id, nombre, `contraseña` FROM usuarios";
$result = $conn->query($sql);

if ($result->num_rows > 0) {
  // output data of each row
  while($row = $result->fetch_assoc()) {
    echo "id: " . $row["id"]. " - Nombre: " . $row["nombre"]. " - Contraseña: " . $row["contraseña"]. "<br>";
  }
} else {
  echo "0 results";
}
$conn->close();
?>