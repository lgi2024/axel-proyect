<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Iniciar Sesión</title>
    <link rel="stylesheet" href="css/styles.css"> <!-- Reutilizamos el mismo archivo de estilos -->
</head>
<body>
    <header>
        <h1><a href="index.html" class="header-link">CRUD Estudiantes 2024</a></h1>
        <div class="nav-buttons">
            <button onclick="window.location.href='login_page.php'">Iniciar sesión</button>
            <button onclick="window.location.href='signin_page.php'">Crear usuario</button>
        </div>
    </header>
    <main>
        <div class="login-box">
            <h2>Iniciar Sesión</h2>
            <form action="./actions/authenticate.php" method="POST" class="login-form">
                <label for="username_or_email">Nombre de Usuario o Correo:</label>
                <input type="text" id="username_or_email" name="username_or_email" class="input-field" required>
                
                <label for="password">Contraseña:</label>
                <input type="password" id="password" name="password" class="input-field" required>
                
                <p><a href="signin_page.php" class="link">Crear una cuenta</a></p> <!-- Link para crear una cuenta -->
                <button type="submit" class="nav-button">Iniciar Sesión</button> <!-- Botón con el mismo estilo -->
            </form>
        </div>
    </main>
    <footer>
        <p>Copyright - IAes Puerto Rico, Misiones, Argentina. Lenguaje Gen. Informes 2024</p>
    </footer>
</body>
</html>

