<?php
// Conexión a la base de datos
include '../database/db_connection.php';

// Configurar el encabezado para que la respuesta sea JSON
header('Content-Type: application/json');

// Recibir datos del formulario
$nombre = $_POST['nombre'];
$apellido = $_POST['apellido'];
$fecha_nac = $_POST['fecha_nac'];
$telefono = $_POST['telefono'];
$direccion = $_POST['direccion'];
$detalles = $_POST['detalles'];

// Procesar la foto si se sube
$foto = '';
if (isset($_FILES['foto']) && $_FILES['foto']['error'] == 0) {
    $foto = basename($_FILES['foto']['name']);
    $target_dir = "../uploads/";
    $target_file = $target_dir . $foto;
    if (!file_exists($target_dir)) {
        mkdir($target_dir, 0777, true);
    }
    move_uploaded_file($_FILES['foto']['tmp_name'], $target_file);
}

// Preparar la consulta SQL
$sql = "INSERT INTO alumnos (nombre, apellido, fecha_nac, telefono, direccion, foto, detalles)
        VALUES (?, ?, ?, ?, ?, ?, ?)";

$stmt = $conn->prepare($sql);
$stmt->bind_param("sssssss", $nombre, $apellido, $fecha_nac, $telefono, $direccion, $foto, $detalles);

if ($stmt->execute()) {
    // Devolver una respuesta JSON si el registro es exitoso
    echo json_encode(['success' => true, 'message' => 'Alumno guardado exitosamente.']);
} else {
    // Devolver una respuesta JSON si hay un error
    echo json_encode(['success' => false, 'error' => 'Error al guardar el alumno: ' . $stmt->error]);
}

// Cerrar la conexión
$stmt->close();
$conn->close();
?>
