<?php
header('Content-Type: application/json');

// Conexión a la base de datos
include '../database/db_connection.php';

// Obtener el ID del alumno a eliminar
$idAlumno = $_POST['id'];

if ($idAlumno) {
    $sql = "DELETE FROM alumnos WHERE id = ?";
    $stmt = $conn->prepare($sql);
    $stmt->bind_param("i", $idAlumno);

    if ($stmt->execute()) {
        echo json_encode(['success' => true]);
    } else {
        echo json_encode(['success' => false, 'error' => 'Error al eliminar el alumno: ' . $stmt->error]);
    }

    $stmt->close();
    $conn->close();
} else {
    echo json_encode(['success' => false, 'error' => 'ID del alumno no proporcionado.']);
}
?>
