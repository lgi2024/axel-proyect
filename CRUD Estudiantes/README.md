Utilice 'database/1_create_db_and_tables.php' y 'database/2_create_basic_users' para crear la base de datos y cargar los usuarios principales admin e invitado. 

Nombre de usuario: admin
Contraseña: admin

Nombre de usuario: invitado
Contraseña: invitado

Para cargar rapidamente datos aleatorios a la base de datos utilice el siguiente código SQL desde PHPMyAdmin:

INSERT INTO alumnos (nombre, apellido, fecha_nac, telefono, direccion, foto, detalles) VALUES
('Sofía', 'González', '2003-07-15', '3012345678', 'Calle de la Luna 123, Barrio Santa Fe, Buenos Aires', 'https://randomuser.me/api/portraits/women/1.jpg', 'Estudiante de 3er año de primaria, muy aplicada en ciencias'),
('Mateo', 'Fernández', '2004-11-22', '3023456789', 'Avenida Belgrano 4567, CABA', 'https://randomuser.me/api/portraits/men/2.jpg', 'Estudiante de 4to año de secundaria, le gusta el fútbol'),
('Valentina', 'López', '2005-01-30', '3034567890', 'Calle 9 de Julio 789, Rosario', 'https://randomuser.me/api/portraits/women/3.jpg', 'Estudiante con buenas calificaciones en matemáticas y arte'),
('Lucas', 'Martínez', '2002-03-25', '3045678901', 'Calle San Martín 1123, Córdoba', 'https://randomuser.me/api/portraits/men/4.jpg', 'Estudiante de 5to año de secundaria, es muy sociable'),
('Camila', 'Rodríguez', '2003-08-19', '3056789012', 'Calle Rivadavia 1500, Mendoza', 'https://randomuser.me/api/portraits/women/5.jpg', 'Estudiante aplicada y responsable, le gusta leer novelas'),
('Tomás', 'Pérez', '2001-12-01', '3067890123', 'Avenida Libertador 1000, Buenos Aires', 'https://randomuser.me/api/portraits/men/6.jpg', 'Estudiante de 1er año de universidad, apasionado por la ingeniería'),
('Isabella', 'Martínez', '2003-05-06', '3078901234', 'Calle Colón 678, Tucumán', 'https://randomuser.me/api/portraits/women/6.jpg', 'Estudiante de secundaria, su deporte favorito es la natación'),
('Benjamín', 'García', '2004-09-10', '3089012345', 'Avenida 25 de Mayo 350, La Plata', 'https://randomuser.me/api/portraits/men/7.jpg', 'Le gusta la tecnología y la programación, se interesa por los videojuegos'),
('Emma', 'Sánchez', '2002-06-28', '3090123456', 'Calle Santa Fe 987, San Juan', 'https://randomuser.me/api/portraits/women/7.jpg', 'Estudiante con buenas habilidades en idiomas extranjeros'),
('Juan', 'Hernández', '2003-10-17', '3101234567', 'Calle Moreno 2345, Rosario', 'https://randomuser.me/api/portraits/men/8.jpg', 'Estudiante de secundaria, tiene interés por la música y la fotografía');

