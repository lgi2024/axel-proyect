// Añadir evento para abrir el modal
document.getElementById("add-student-button").addEventListener("click", openAddStudentModal);

function openAddStudentModal() {
    document.getElementById("addStudentModal").style.display = "flex"; // Cambiado a "flex" para mostrar el modal centrado
}

function closeAddStudentModal() {
    document.getElementById("addStudentModal").style.display = "none";
}

// Cerrar el modal si se hace clic fuera de él
window.onclick = function(event) {
    let modal = document.getElementById("addStudentModal");
    if (event.target === modal) {
        closeAddStudentModal();
    }
}

// Función para guardar el alumno usando AJAX
function guardarAlumno() {
    // Crear un objeto FormData con los datos del formulario
    const form = document.getElementById("addStudentForm");
    const formData = new FormData(form);

    // Enviar el formulario con fetch a la base de datos
    fetch("./includes/save_student.php", {
        method: "POST",
        body: formData
    })
    .then(response => response.json()) // Espera una respuesta JSON desde PHP
    .then(data => {
        if (data.success) {
            alert("Alumno guardado"); // Mensaje de confirmación
            closeAddStudentModal();   // Cerrar el modal
            form.reset();             // Limpiar el formulario
        } else {
            alert("Error al guardar el alumno: " + data.error);
        }
    })
    .catch(error => console.error("Error:", error));
}