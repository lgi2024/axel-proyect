<?php
// Conectar a MySQL
$servername = "localhost";
$username = "root"; // Cambia esto si tienes un usuario diferente
$password = ""; // Cambia esto si tienes una contraseña

// Crear conexión
$conn = new mysqli($servername, $username, $password);

// Verificar conexión
if ($conn->connect_error) {
    die("Error de conexión: " . $conn->connect_error);
}

// Crear base de datos 'escuela'
$sql = "CREATE DATABASE IF NOT EXISTS escuela";
if ($conn->query($sql) === TRUE) {
    echo "Base de datos 'escuela' creada exitosamente.<br>";
} else {
    echo "Error al crear base de datos: " . $conn->error;
}

// Seleccionar la base de datos
$conn->select_db("escuela");

// Crear tabla 'alumnos'
$sql_alumnos = "CREATE TABLE IF NOT EXISTS alumnos (
    id INT(11) AUTO_INCREMENT PRIMARY KEY,
    nombre VARCHAR(50) NOT NULL,
    apellido VARCHAR(50) NOT NULL,
    fecha_nac DATE NOT NULL,
    telefono VARCHAR(15),
    direccion VARCHAR(255),
    foto VARCHAR(255),
    detalles TEXT
)";
if ($conn->query($sql_alumnos) === TRUE) {
    echo "Tabla 'alumnos' creada exitosamente.<br>";
} else {
    echo "Error al crear tabla 'alumnos': " . $conn->error;
}

// Crear tabla 'usuarios'
$sql_usuarios = "CREATE TABLE IF NOT EXISTS usuarios (
    id INT(11) AUTO_INCREMENT PRIMARY KEY,
    nombre VARCHAR(50) NOT NULL,
    password VARCHAR(255) NOT NULL,
    email VARCHAR(100) NOT NULL UNIQUE,
    type ENUM('administrador', 'invitado') NOT NULL
)";
if ($conn->query($sql_usuarios) === TRUE) {
    echo "Tabla 'usuarios' creada exitosamente.<br>";
} else {
    echo "Error al crear tabla 'usuarios': " . $conn->error;
}

// Cerrar conexión
$conn->close();
?>
