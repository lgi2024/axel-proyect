<?php
$servername = "localhost";
$db_username = "root"; // Cambia esto si tienes otro usuario
$db_password = ""; // Cambia esto si tienes contraseña
$database = "escuela"; // Nombre de la base de datos

// Crear conexión
$conn = new mysqli($servername, $db_username, $db_password, $database);

// Verificar conexión
if ($conn->connect_error) {
    die("Conexión fallida: " . $conn->connect_error);
}