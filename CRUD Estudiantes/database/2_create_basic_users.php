<?php
// Configuración de conexión a la base de datos
$servername = "localhost";
$username = "root"; // Cambia esto si tienes otro usuario
$password = ""; // Cambia esto si tienes contraseña
$database = "escuela"; // Nombre de la base de datos

// Crear conexión
$conn = new mysqli($servername, $username, $password, $database);

// Verificar conexión
if ($conn->connect_error) {
    die("Conexión fallida: " . $conn->connect_error);
}

// Consultas para insertar usuarios
$sql_guest = "INSERT INTO usuarios (nombre, password, email, type) VALUES ('invitado', 'invitado', 'guest@gmail.com', 'invitado')";
$sql_admin = "INSERT INTO usuarios (nombre, password, email, type) VALUES ('admin', 'admin', 'admin@gmail.com', 'administrador')";

// Ejecutar las consultas
if ($conn->query($sql_guest) === TRUE) {
    echo "Usuario 'invitado' agregado exitosamente.<br>";
} else {
    echo "Error al agregar usuario 'invitado': " . $conn->error . "<br>";
}

if ($conn->query($sql_admin) === TRUE) {
    echo "Usuario 'admin' agregado exitosamente.<br>";
} else {
    echo "Error al agregar usuario 'admin': " . $conn->error . "<br>";
}

// Cerrar conexión
$conn->close();
?>
