<?php
session_start();

// Verificar si el usuario ha iniciado sesión
if (!isset($_SESSION['loggedin']) || $_SESSION['loggedin'] !== true) {
    header("Location: login_page.php"); // Redirigir a la página de inicio de sesión
    exit;
}

// Obtener el tipo de usuario
$user_type = $_SESSION['type'];
$username = $_SESSION['username'];


// Re utiliza el codigo de conexion
include 'database/db_connection.php';


// Crear conexión
$conn = new mysqli($servername, $db_username, $db_password, $database);

// Verificar conexión
if ($conn->connect_error) {
    die("Conexión fallida: " . $conn->connect_error);
}

// Manejo de la búsqueda
$search_query = isset($_POST['search']) ? $_POST['search'] : '';

// Preparar la consulta
$sql = "SELECT * FROM alumnos WHERE nombre LIKE ? OR apellido LIKE ?";
$stmt = $conn->prepare($sql);
$like_query = "%" . $search_query . "%";
$stmt->bind_param("ss", $like_query, $like_query);
$stmt->execute();
$result = $stmt->get_result();
?>
<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>CRUD 2024</title>
    <link rel="stylesheet" href="css/styles.css"> <!-- Reutilizamos el mismo archivo de estilos -->
    <script>
        function confirmarEliminacion(idAlumno) {
           if (confirm("¿Estás seguro de que deseas eliminar este alumno?")) {
                fetch('./includes/delete_student.php', {
                    method: 'POST',
                    headers: {
                        'Content-Type': 'application/x-www-form-urlencoded'
                    },
                    body: `id=${idAlumno}`
                })
                .then(response => response.json())
                .then(data => {
                    if (data.success) {
                        alert("Alumno eliminado correctamente.");
                        // Opcional: recargar la lista de alumnos sin refrescar la página
                        location.reload(); // Recarga la página para reflejar los cambios
                    } else {
                        alert("Error al eliminar el alumno: " + data.error);
                    }
                })
                .catch(error => console.error("Error:", error));
            }
        }
    </script>
</head>
<body>
    <header>
        <h1><a href="main_page.php" class="header-link">CRUD Estudiantes 2024</a></h1>
        <div class="nav-buttons">
            <span><?php echo htmlspecialchars($username); ?></span>
            <button onclick="window.location.href='./actions/logout.php'">Cerrar sesión</button>
        </div>
    </header>
    <main>
        <div class="search-bar-container">
            <form method="POST" action="" class="search-form">
                <input type="text" name="search" placeholder="Buscar alumno..." class="search-input" required>
                <button type="submit" class="search-button">Buscar</button>
            </form>
        </div>
        <div class="students-list-container">
            <div class="students-list-header">
                <h2>Lista de Alumnos</h2>
                <?php if ($user_type === 'administrador'): ?>
                    <button id="add-student-button" class="add-student-button">Añadir Alumno</button>
                    <div id="addStudentModal" class="modal" style="display: none;">
                        <div class="modal-content">
                            <span class="close-button" onclick="closeAddStudentModal()">&times;</span>
                            <h2>Añadir Alumno</h2>
                            <form id="addStudentForm" enctype="multipart/form-data">
                                
                                <label for="nombre">Nombre:</label>
                                <input type="text" id="nombre" name="nombre" required>

                                <label for="apellido">Apellido:</label>
                                <input type="text" id="apellido" name="apellido" required>

                                <label for="fecha_nac">Fecha de Nacimiento:</label>
                                <input type="date" id="fecha_nac" name="fecha_nac" required>

                                <label for="telefono">Teléfono:</label>
                                <input type="text" id="telefono" name="telefono" required>

                                <label for="direccion">Dirección:</label>
                                <input type="text" id="direccion" name="direccion" required>

                                <label for="foto">Foto:</label>
                                <input type="file" id="foto" name="foto">

                                <label for="detalles">Detalles:</label>
                                <textarea id="detalles" name="detalles"></textarea>

                                <button type="button" onclick="guardarAlumno()">Guardar Alumno</button>
                            </form>
                        </div>
                    </div>
                <?php endif; ?>
            </div>
            <ul class="students-list">
                <?php while ($row = $result->fetch_assoc()): ?>
                    <li class="student-item">
                        <?php echo htmlspecialchars($row['nombre'] . ' ' . $row['apellido']); ?>
                        <div class="student-actions">
                            <?php if ($user_type === 'administrador'): ?>
                                <button onclick="window.location.href='detail_student.php?id=<?php echo $row['id']; ?>'">Ver alumno</button>
                                <button onclick="window.location.href='edit_student.php?id=<?php echo $row['id']; ?>'">Editar alumno</button>
                                <button onclick="confirmarEliminacion(<?php echo $row['id']; ?>)">Borrar alumno</button>
                            <?php endif; ?>
                        </div>
                    </li>
                <?php endwhile; ?>
            </ul>
        </div>
    </main>
    <footer>
        <p>Copyright - IAes Puerto Rico, Misiones, Argentina. Lenguaje Gen. Informes 2024</p>
    </footer>
    </div>
<script src="js/script.js"></script>
</body>
</html>




