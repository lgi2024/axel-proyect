<?php
// Configuración de conexión a la base de datos
$servername = "localhost";
$username = "root"; // Cambia esto si tienes otro usuario
$password = ""; // Cambia esto si tienes contraseña
$database = "escuela"; // Nombre de la base de datos

// Crear conexión
$conn = new mysqli($servername, $username, $password, $database);

// Comprobar conexión
if ($conn->connect_error) {
    die("Conexión fallida: " . $conn->connect_error);
}

// Iniciar la sesión
session_start();

// Procesar la solicitud POST
if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    // Verificar si los campos están establecidos
    if (isset($_POST['username_or_email']) && isset($_POST['password'])) {
        $username_or_email = $_POST['username_or_email'];
        $password = $_POST['password'];

        // Preparar y ejecutar la consulta para verificar tanto el nombre de usuario como el correo
        $stmt = $conn->prepare("SELECT nombre, password, type FROM usuarios WHERE nombre = ? OR email = ?");
        $stmt->bind_param("ss", $username_or_email, $username_or_email);
        $stmt->execute();
        $result = $stmt->get_result();

        // Verificar si se encontró el usuario
        if ($result->num_rows === 1) {
            $row = $result->fetch_assoc();
            $stored_password = $row['password'];

            // Verificar la contraseña
            if ($stored_password === $password) {
                // Contraseña correcta, almacenar datos en la sesión
                $_SESSION['loggedin'] = true;
                $_SESSION['username'] = $row['nombre'];
                $_SESSION['type'] = $row['type'];

                // Redirigir a la página principal
                header("Location: ../main_page.php");
                exit();
            } else {
                echo "Contraseña incorrecta.";
            }
        } else {
            echo "Usuario no encontrado.";
        }

        $stmt->close();
    } else {
        echo "No se recibieron todos los datos.";
    }
} else {
    echo "Método no permitido.";
}

// Cerrar conexión
$conn->close();
?>
