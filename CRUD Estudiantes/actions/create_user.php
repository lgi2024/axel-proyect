<?php
// Configuración de conexión a la base de datos
$servername = "localhost";
$username = "root"; // Cambia esto si tienes otro usuario
$password = ""; // Cambia esto si tienes contraseña
$database = "escuela"; // Nombre de la base de datos

// Crear conexión
$conn = new mysqli($servername, $username, $password, $database);

// Comprobar conexión
if ($conn->connect_error) {
    die("Conexión fallida: " . $conn->connect_error);
}

// Procesar la solicitud POST
if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    // Comprobar si los campos están establecidos
    if (isset($_POST['username'], $_POST['email'], $_POST['password'])) {
        $nombre = $_POST['username'];
        $email = $_POST['email'];
        $password = $_POST['password']; // Almacenar la contraseña sin encriptar
        $type = 'invitado'; // Asignar siempre como "invitado"

        // Preparar y ejecutar la consulta
        $stmt = $conn->prepare("INSERT INTO usuarios (nombre, password, email, type) VALUES (?, ?, ?, ?)");
        $stmt->bind_param("ssss", $nombre, $password, $email, $type); // Usar la contraseña sin encriptar

        if ($stmt->execute()) {
            echo "Usuario creado exitosamente.";
            // Redirigir a otra página o mostrar mensaje
            header("Location: ../main_page.php"); // Cambia a la página que desees
            exit();
        } else {
            echo "Error: " . $stmt->error;
        }

        $stmt->close();
    } else {
        echo "No se recibieron todos los datos.";
    }
} else {
    echo "Método no permitido.";
}

// Cerrar conexión
$conn->close();
?>
