<?php
// Configuración de conexión a la base de datos
$servername = "localhost";
$username = "root";
$password = "";
$database = "escuela";

// Crear conexión
$conn = new mysqli($servername, $username, $password, $database);

// Comprobar conexión
if ($conn->connect_error) {
    die("Conexión fallida: " . $conn->connect_error);
}

// Procesar la solicitud POST
if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    if (isset($_POST['username'], $_POST['email'], $_POST['password'])) {
        $nombre = $_POST['username'];
        $email = $_POST['email'];
        $password = $_POST['password'];
        $type = 'invitado';

        // Preparar y ejecutar la consulta
        $stmt = $conn->prepare("INSERT INTO usuarios (nombre, password, email, type) VALUES (?, ?, ?, ?)");
        $stmt->bind_param("ssss", $nombre, $password, $email, $type);
        if ($stmt->execute()) {
            // Iniciar sesión automáticamente después de crear el usuario
            session_start();
            $_SESSION['username'] = $nombre;
            $_SESSION['user_type'] = $type;
            // Redirigir a la página principal
            header("Location: main_page.php");
            exit();
        } else {
            echo "Error: " . $stmt->error;
        }
        $stmt->close();
    } else {
        echo "No se recibieron todos los datos.";
    }
}
$conn->close();


?>
<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Crear Usuario</title>
    <link rel="stylesheet" href="css/styles.css"> <!-- Reutilizamos el mismo archivo de estilos -->
</head>
<body>
    <header>
        <h1><a href="start_page.html" class="header-link">CRUD Estudiantes 2024</a></h1>
        <div class="nav-buttons">
            <button onclick="window.location.href='login_page.php'">Iniciar sesión</button>
            <button onclick="window.location.href='signin_page.php'">Crear usuario</button>
        </div>
    </header>
    <main>
    <div class="login-box">
    <h2>Crear Usuario</h2>
    <form class="login-form" action="actions/create_user.php" method="POST">
        <label for="username">Nombre de usuario</label>
        <input type="text" id="username" name="username" class="input-field" required>
        
        <label for="password">Contraseña</label>
        <input type="password" id="password" name="password" class="input-field" required>
        
        <label for="email">Correo electrónico</label>
        <input type="email" id="email" name="email" class="input-field" required>
        
        <button type="submit">Crear Usuario</button>
    </form>
</div>
    </main>
    <footer>
        <p>Copyright - IAes Puerto Rico, Misiones, Argentina. Lenguaje Gen. Informes 2024</p>
    </footer>
</body>
</html>
