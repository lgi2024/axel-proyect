<?php 
session_start(); 

// Redirige a index si la sesión no existe
if (!isset($_SESSION['user_id'])) { 
    header("Location: index.php"); 
    exit(); 
}

// Verifica el rol del usuario
$role = $_SESSION['user_role']; 
if ($role !== 'administrador') {
    header("Location: main.php"); 
    exit(); 
}
?>

<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Editar Stock</title>
    <link rel="stylesheet" href="css/main.css">
</head>
<body>
    <header>
        <h1>Estadisticas</h1>
        <div class="nav-buttons">
            <span>Bienvenido, <?php echo htmlspecialchars($_SESSION['user_name']); ?></span>
            <button onclick="window.location.href='./actions/logout.php'">Cerrar sesión</button>
        </div>
    </header>
    <nav class="nav-bar">
        <button id="inicioBtn" onclick="window.location.href='main.php'">Inicio</button>
        <button id="editarStockBtn" onclick="window.location.href='editar_stock.php'">Editar stock</button>
        <button id="estadisticasBtn" class="active">Estadisticas</button>


        
    </nav>
    <main>
        <!--  <h2>Editar Stock</h2> -->
       
    </main>
    <footer>
        <p>© Copyright - IAes Puerto Rico, Misiones, Argentina. Lenguaje Gen. Informes 2024</p>
    </footer>
    <script src="js/jquery-3.6.0.min.js"></script>
    <script src="js/editScripts.js" defer></script>
</body>
</html>
