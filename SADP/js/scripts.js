document.addEventListener("DOMContentLoaded", function () {
    const loginBtn = document.getElementById("login-btn");
    const createAccountBtn = document.getElementById("create-account-btn");
    const modalLogin = document.getElementById("modal-login");
    const modalCreateAccount = document.getElementById("modal-create-account");
    const closeModalButtons = document.querySelectorAll(".modal-close");
    const switchToCreate = document.getElementById("switch-to-create");
    const switchToLogin = document.getElementById("switch-to-login");

    if (loginBtn) {
        loginBtn.addEventListener("click", function () {
            modalLogin.style.display = "flex";
        });
    }

    if (createAccountBtn) {
        createAccountBtn.addEventListener("click", function () {
            modalCreateAccount.style.display = "flex";
        });
    }

    closeModalButtons.forEach((button) => {
        button.addEventListener("click", function () {
            button.closest(".modal").style.display = "none";
        });
    });

    if (switchToCreate) {
        switchToCreate.addEventListener("click", function (e) {
            e.preventDefault();
            modalLogin.style.display = "none";
            modalCreateAccount.style.display = "flex";
        });
    }

    if (switchToLogin) {
        switchToLogin.addEventListener("click", function (e) {
            e.preventDefault();
            modalCreateAccount.style.display = "none";
            modalLogin.style.display = "flex";
        });
    }

    // Manejo de formularios
    const createAccountForm = document.getElementById("create-account-form");
    if (createAccountForm) {
        createAccountForm.addEventListener("submit", function (e) {
            e.preventDefault();
            const formData = new FormData(createAccountForm);
            fetch('./actions/create_account.php', {
                method: 'POST',
                body: formData
            })
            .then(response => {
                if (!response.ok) {
                    throw new Error('Error en el servidor: ' + response.status);
                }
                return response.json();
            })
            .then(data => {
                if (data.success) {
                    // alert("Cuenta creada exitosamente. Iniciando sesión...");
                    window.location.href = 'main.php';
                } else {
                    document.getElementById("error-message").textContent = data.message;
                    document.getElementById("error-message").style.display = "block";
                }
            })
            .catch(error => {
                console.error('Error:', error);
                document.getElementById("error-message").textContent = 'Error en el servidor. Por favor, inténtelo de nuevo más tarde.';
                document.getElementById("error-message").style.display = "block";
            });
        });
    }

    const loginForm = document.getElementById("login-form");
    if (loginForm) {
        loginForm.addEventListener("submit", function (e) {
            e.preventDefault();
            const formData = new FormData(loginForm);
            const existingError = document.getElementById("login-error-message");
            if (existingError) {
                existingError.remove();
            }
            fetch('./actions/login.php', {
                method: 'POST',
                body: formData
            })
            .then(response => {
                if (!response.ok) {
                    throw new Error('Error en el servidor: ' + response.status);
                }
                return response.json();
            })
            .then(data => {
                if (data.success) {
                    // alert("Inicio de sesión exitoso. Redirigiendo...");
                    window.location.href = 'main.php';
                } else {
                    const loginErrorMessage = document.createElement("span");
                    loginErrorMessage.className = "error-message";
                    loginErrorMessage.id = "login-error-message";
                    loginErrorMessage.textContent = data.message;
                    document.getElementById("login-form").appendChild(loginErrorMessage);
                }
            })
            .catch(error => {
                console.error('Error:', error);
                const loginErrorMessage = document.createElement("span");
                loginErrorMessage.className = "error-message";
                loginErrorMessage.id = "login-error-message";
                loginErrorMessage.textContent = 'Error en el servidor. Por favor, inténtelo de nuevo más tarde.';
                document.getElementById("login-form").appendChild(loginErrorMessage);
            });
        });
    }

    //------------------------------------------------------------------------------------------------------------



    // Paginación y renderización de productos
    let currentPage = 1;

    function fetchProducts(page = 1) {
        $.ajax({
            url: 'database/utils/get_products.php',
            type: 'GET',
            data: { page },
            success: function (response) {
                if (response.error) {
                    console.error("Error del servidor:", response.error);
                    alert("Ocurrió un error al cargar los productos.");
                    return;
                }
                renderProducts(response.products);
                renderPagination(response.totalPages, page);
            },
            error: function () {
                alert('Error al comunicarse con el servidor.');
            }
        });
    }

    function renderProducts(products) {
        const mainContent = $('#mainContent');
        mainContent.empty();

        if (products.length === 0) {
            mainContent.html('<p>No hay productos disponibles.</p>');
            return;
        }

        let table = `
            <table class="products-table">
                <thead>
                    <tr>
                        <th>Nombre</th>
                        <th>Descripción</th>
                        <th>Precio</th>
                        <th>Stock</th>
                    </tr>
                </thead>
                <tbody>
        `;

        products.forEach(product => {
            let precio = parseFloat(product.precio);
            if (isNaN(precio)) {
                precio = 0;
            }
            table += `
                <tr>
                    <td>${product.nombre_producto}</td>
                    <td>${product.descripcion}</td>
                    <td>$${precio.toFixed(2)}</td>
                    <td>${product.stock}</td>
                </tr>
            `;
        });

        table += `</tbody></table>`;
        mainContent.html(table);
    }

    function renderPagination(totalPages, currentPage) {
        const pagination = $('<div class="pagination"></div>');
        for (let i = 1; i <= totalPages; i++) {
            const button = $(`<button>${i}</button>`);
            if (i === currentPage) button.addClass('active');
            button.on('click', () => fetchProducts(i));
            pagination.append(button);
        }
        $('#mainContent').append(pagination);
    }

    const inicioBtn = $('#inicioBtn');
    if (inicioBtn.length > 0) {
        inicioBtn.on('click', () => {
            currentPage = 1;
            fetchProducts();
        });
    } else {
        console.error("El botón 'Inicio' no se encuentra en el DOM.");
    }

    fetchProducts();
});


// -------------- Editar - Stock ----------------------//

