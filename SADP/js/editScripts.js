// Función para cargar y renderizar productos con opciones para editar el stock
function fetchAndRenderEditableProducts(page = 1) {
    $.ajax({
        url: 'database/utils/get_products.php',
        type: 'GET',
        data: { page },
        success: function (response) {
            if (response.error) {
                console.error("Error del servidor:", response.error);
                alert("Ocurrió un error al cargar los productos.");
                return;
            }
            renderEditableProducts(response.products);
            renderEditablePagination(response.totalPages, page);
        },
        error: function () {
            alert('Error al comunicarse con el servidor.');
        }
    });
}

function renderEditableProducts(products) {
    const editContent = $('#editContent');
    editContent.empty();

    if (products.length === 0) {
        editContent.html('<p>No hay productos disponibles.</p>');
        return;
    }

    let table = `
        <table class="products-table">
            <thead>
                <tr>
                    <th>Nombre</th>
                    <th>Descripción</th>
                    <th>Precio</th>
                    <th>Stock</th>
                    <th>Acciones</th>
                </tr>
            </thead>
            <tbody>
    `;

    products.forEach(product => {
        let precio = parseFloat(product.precio);
        if (isNaN(precio)) {
            precio = 0;
        }
        table += `
            <tr>
                <td>${product.nombre_producto}</td>
                <td>${product.descripcion}</td>
                <td>$${precio.toFixed(2)}</td>
                <td>${product.stock}</td>
                <td>
                    <button onclick="openEditModal(${product.id_producto}, '${product.nombre_producto}', '${product.descripcion}', ${precio}, ${product.stock})">Editar</button>
                </td>
            </tr>
        `;
    });

    table += `</tbody></table>`;
    editContent.html(table);
}

function renderEditablePagination(totalPages, currentPage) {
    const pagination = $('<div class="pagination"></div>');
    for (let i = 1; i <= totalPages; i++) {
        const button = $(`<button>${i}</button>`);
        if (i === currentPage) button.addClass('active');
        button.on('click', () => fetchAndRenderEditableProducts(i));
        pagination.append(button);
    }
    $('#editContent').append(pagination);
}

// Abrir la ventana modal con los datos del producto
function openEditModal(id, nombre, descripcion, precio, stock) {
    $('#editModal').show();
    $('#productId').val(id);
    $('#productName').val(nombre);
    $('#productDescription').val(descripcion);
    $('#productPrice').val(precio);
    $('#productStock').val(stock);
}

// Cerrar la ventana modal
function closeEditModal() {
    $('#editModal').hide();
    $('#editForm')[0].reset();
}

function updateProduct() {
    const id = $('#productId').val();
    const nombre = $('#productName').val();
    const descripcion = $('#productDescription').val();
    const precio = parseFloat($('#productPrice').val());
    const stock = parseInt($('#productStock').val());

    console.log("Datos enviados: ", {
        id,
        nombre,
        descripcion,
        precio,
        stock
    });

    $.ajax({
        url: 'database/utils/update_product.php',
        type: 'POST',
        data: {
            id,
            nombre,
            descripcion,
            precio,
            stock
        },
        success: function (response) {
            if (response.error) {
                console.error("Error del servidor:", response.error);
                alert("Ocurrió un error al actualizar el producto. " + response.error);
                return;
            }
            closeEditModal();
            fetchAndRenderEditableProducts();
        },
        error: function (xhr, status, error) {
            alert('Error al comunicarse con el servidor: ' + xhr.responseText);
            console.error('Error al comunicarse con el servidor:', xhr, status, error);
        }
    });
}

$(document).ready(function() {
    fetchAndRenderEditableProducts();

    $('#closeModalBtn').on('click', closeEditModal);
    $('#editForm').on('submit', function(e) {
        e.preventDefault();
        updateProduct();
    });
});

// ----------------------------------------------------------------------------


// Función para mostrar el anuncio
function showNotification(message) {
    const notification = $('<div class="notification"></div>').text(message);
    $('body').append(notification);
    setTimeout(() => {
        notification.fadeOut(300, () => {
            notification.remove();
        });
    }, 2000);
}

function updateProduct() {
    const id = $('#productId').val();
    const nombre = $('#productName').val();
    const descripcion = $('#productDescription').val();
    const precio = parseFloat($('#productPrice').val());
    const stock = parseInt($('#productStock').val());

    console.log("Datos enviados: ", {
        id,
        nombre,
        descripcion,
        precio,
        stock
    });

    $.ajax({
        url: 'database/utils/update_product.php',
        type: 'POST',
        data: {
            id,
            nombre,
            descripcion,
            precio,
            stock
        },
        success: function (response) {
            if (response.error) {
                console.error("Error del servidor:", response.error);
                alert("Ocurrió un error al actualizar el producto. " + response.error);
                return;
            }
            closeEditModal();
            fetchAndRenderEditableProducts();
            showNotification('Producto actualizado correctamente.');
        },
        error: function (xhr, status, error) {
            alert('Error al comunicarse con el servidor: ' + xhr.responseText);
            console.error('Error al comunicarse con el servidor:', xhr, status, error);
        }
    });
}

$(document).ready(function() {
    fetchAndRenderEditableProducts();

    $('#closeModalBtn').on('click', closeEditModal);
    $('#editForm').on('submit', function(e) {
        e.preventDefault();
        updateProduct();
    });
});


// Nuevo código añadido para manejar la adición de productos
function openAddModal() {
    $('#editModal').show();
    $('#productId').val('');
    $('#productName').val('');
    $('#productDescription').val('');
    $('#productPrice').val('');
    $('#productStock').val('');
    $('#modalTitle').text('Añadir Producto');
    $('#submitBtn').text('Añadir Producto');
}

function addProduct() {
    const nombre = $('#productName').val();
    const descripcion = $('#productDescription').val();
    const precio = parseFloat($('#productPrice').val());
    const stock = parseInt($('#productStock').val());

    console.log("Datos enviados: ", {
        nombre,
        descripcion,
        precio,
        stock
    });

    $.ajax({
        url: 'database/utils/add_product.php',
        type: 'POST',
        data: {
            nombre,
            descripcion,
            precio,
            stock
        },
        success: function (response) {
            if (response.error) {
                console.error("Error del servidor:", response.error);
                alert("Ocurrió un error al añadir el producto. " + response.error);
                return;
            }
            closeEditModal();
            fetchAndRenderEditableProducts();
            showNotification('Producto añadido correctamente.');
        },
        error: function (xhr, status, error) {
            alert('Error al comunicarse con el servidor: ' + xhr.responseText);
            console.error('Error al comunicarse con el servidor:', xhr, status, error);
        }
    });
}

// Evento para abrir el modal de añadir producto
$(document).ready(function() {
    $('#addProductBtn').on('click', openAddModal);
});
