<?php 
session_start(); 

// Redirige a index si la sesión no existe (no funciona)
if (!isset($_SESSION['user_id'])) { 
    header("Location: index.php"); 
    exit(); 
}

$role = $_SESSION['user_role']; 
?>


<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Sistema Administrador</title>
    <link rel="stylesheet" href="css/main.css">
    
</head>
<body>
    <header>
        <h1>Productos</h1>
        <div class="nav-buttons">
            <span>Bienvenido, <?php echo htmlspecialchars($_SESSION['user_name']); ?></span>
            <button onclick="window.location.href='./actions/logout.php'">Cerrar sesión</button>
        </div>
    </header>
    <nav class="nav-bar">
        <button id="inicioBtn" class="active" onclick="window.location.href='main.php'">Inicio</button>
        <?php if ($role === 'administrador'): ?>
            <button id="editarStockBtn" onclick="window.location.href='editar_stock.php'">Editar stock</button>
            <button id="estadisticasBtn" onclick="window.location.href='estadisticas.php'">Estadísticas</button>
        <?php endif; ?>
    </nav>

    <main id="mainContent"></main>
    <footer>
        <p>© Copyright - IAes Puerto Rico, Misiones, Argentina. Lenguaje Gen. Informes 2024</p>
    </footer>
    <script src="js/jquery-3.6.0.min.js"></script>
    <script src="js/scripts.js" defer></script>
</body>
</html>
