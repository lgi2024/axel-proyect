<?php 
session_start(); 

// Redirige a index si la sesión no existe
if (!isset($_SESSION['user_id'])) { 
    header("Location: index.php"); 
    exit(); 
}

// Verifica el rol del usuario
$role = $_SESSION['user_role']; 
if ($role !== 'administrador') {
    header("Location: main.php"); 
    exit(); 
}
?>

<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Editar Stock</title>
    <link rel="stylesheet" href="css/main.css">
</head>
<body>
    <header>
        <h1>Editar stock</h1>
        <div class="nav-buttons">
            <span>Bienvenido, <?php echo htmlspecialchars($_SESSION['user_name']); ?></span>
            <button onclick="window.location.href='./actions/logout.php'">Cerrar sesión</button>
        </div>
    </header>
    <nav class="nav-bar">
        <button id="inicioBtn" onclick="window.location.href='main.php'">Inicio</button>
        <button id="editarStockBtn" class="active">Editar stock</button>
        <button id="estadisticasBtn" onclick="window.location.href='estadisticas.php'">Estadísticas</button>
    </nav>
    <main id="mainContent">
        <div class="nav-buttons">
            <button id="addProductBtn">Añadir Producto</button>

        </div>
        <div id="editContent"></div>

        <!-- Ventana Modal para Editar/Añadir Producto -->
        <div id="editModal" class="modal">
            <div class="modal-content">
                <span id="closeModalBtn" class="modal-close">&times;</span>
                <h2 id="modalTitle">Editar Producto</h2>
                <form id="editForm">
                    <input type="hidden" id="productId">
                    <div>
                        <label for="productName">Nombre:</label>
                        <input type="text" id="productName" required>
                    </div>
                    <div>
                        <label for="productDescription">Descripción:</label>
                        <input type="text" id="productDescription" required>
                    </div>
                    <div>
                        <label for="productPrice">Precio:</label>
                        <input type="number" step="0.01" id="productPrice" required>
                    </div>
                    <div>
                        <label for="productStock">Stock:</label>
                        <input type="number" id="productStock" required>
                    </div>
                    <div>
                        <button type="submit" id="submitBtn">Actualizar Producto</button>
                    </div>
                </form>
            </div>
        </div>
    </main>
    <footer>
        <p>© Copyright - IAes Puerto Rico, Misiones, Argentina. Lenguaje Gen. Informes 2024</p>
    </footer>
    <script src="js/jquery-3.6.0.min.js"></script>
    <script src="js/editScripts.js" defer></script>
</body>
</html>
