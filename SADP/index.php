<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Inicio</title>
    <link rel="stylesheet" href="css/main.css">  
</head>
<script src="js/scripts.js"></script>
<body>
    <!-- Encabezado principal -->
    <header>
        <h1>Sistema Administrador de Productos</h1>
        <!-- Botones de la esquina superior derecha -->
        <div class="nav-buttons">
            <button id="login-btn">Iniciar sesión</button>
            <button id="create-account-btn">Crear cuenta</button>
        </div>
    </header>

    <!-- Ventana modal para Iniciar Sesión -->
    <div id="modal-login" class="modal">
        <div class="modal-content">
            <span class="modal-close">&times;</span>
            <h2>Iniciar Sesión</h2>
            <form id="login-form">
                <label for="username">Nombre de usuario o correo:</label>
                <input type="text" id="username" name="username" required>
                <span id="username-error" class="error-message"></span>

                <label for="password">Contraseña:</label>
                <input type="password" id="password" name="password" required>
                <span id="password-error" class="error-message"></span>

                <button type="submit">Iniciar</button>
                <p>¿No tienes cuenta? <a href="#" id="switch-to-create">Crear cuenta</a></p>
            </form>
        </div>
    </div>

<!-- Modal Crear Cuenta -->
    <div id="modal-create-account" class="modal">
        <div class="modal-content">
            <span class="modal-close">&times;</span>
            <h2>Crear Cuenta</h2>
            <form id="create-account-form">
                <label for="new-username">Nombre de usuario:</label>
                <input type="text" id="new-username" name="username" required>

                <label for="new-email">Correo electrónico:</label>
                <input type="email" id="new-email" name="email" required>

                <label for="new-password">Contraseña:</label>
                <input type="password" id="new-password" name="password" required>

                <label for="confirm-password">Confirmar Contraseña:</label>
                <input type="password" id="confirm-password" name="confirm-password" required>

                <!-- Mensaje de error -->
                <div id="error-message" class="error-message" style="display: none;">Las contraseñas no coinciden.</div>

                <button type="submit">Crear cuenta</button>
                <p>¿Ya tienes cuenta? <a href="#" id="switch-to-login">Iniciar sesión</a></p>
            </form>
        </div>
    </div>

    <footer>
        <p>© Copyright - IAes Puerto Rico, Misiones, Argentina. Lenguaje Gen. Informes 2024</p>
    </footer>

    

</body>
</html>
