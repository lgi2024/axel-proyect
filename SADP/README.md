 Utilice 'localhost/axel-proyect/SADP/database/SQL/1-create-database.php'
y 'localhost/axel-proyect/SADP/database/SQL/2-load-data.php'
para crear la base de datos, cargar los usuarios y los productos.

- Usuario 1
Nombre: cliente
Pass: cliente
Tipo: Cliente

- Usuario 2
Nombre: admin
Pass: admin
Tipo: administrador

Usuario cliente solo puede visualizar la base de datos. 
Usuario admin puede visualizar, editar la base de datos y ver estadísticas de uso.

Bugs:
- Al crear cuenta no guarda la sesión y main.php redirige a index.php.
- Estadísticas aún no existe.