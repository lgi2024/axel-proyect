<?php
// Incluir el archivo de conexión
require_once '../connection/db_connection.php';

// Insertar datos en la tabla 'usuarios'
$sql = "INSERT INTO usuarios (nombre, email, contraseña, rol) VALUES
    ('admin', 'admin@admin.com', SHA2('admin', 256), 'administrador'),
    ('cliente', 'cliente@cliente.com', SHA2('cliente', 256), 'cliente')";

if ($conn->query($sql) === TRUE) {
    echo "Datos insertados en 'usuarios' exitosamente.<br>";
} else {
    echo "Error al insertar datos en 'usuarios': " . $conn->error . "<br>";
}

// Insertar datos en la tabla 'productos'
$sql = "INSERT INTO productos (nombre_producto, descripcion, precio, stock) VALUES
    ('Laptop', 'Laptop de 15 pulgadas con 8GB RAM y 512GB SSD', 800.00, 10),
    ('Teclado', 'Teclado mecánico RGB con switches rojos', 50.00, 25),
    ('Mouse', 'Mouse inalámbrico con sensor óptico', 20.00, 30),
    ('Monitor', 'Monitor Full HD de 24 pulgadas', 150.00, 15),
    ('Auriculares', 'Auriculares con cancelación de ruido y Bluetooth', 120.00, 20),
    ('Cámara', 'Cámara digital con resolución 4K y pantalla táctil', 500.00, 8),
    ('Parlante', 'Parlante inalámbrico con sonido estéreo y batería de 10 horas', 75.00, 40),
    ('Webcam', 'Webcam Full HD con micrófono integrado', 40.00, 12),
    ('Disco Duro', 'Disco duro externo de 1TB USB 3.0', 60.00, 18),
    ('Tablet', 'Tablet de 10 pulgadas con 64GB de almacenamiento', 250.00, 22),
    ('Smartphone', 'Smartphone Android con pantalla de 6.5 pulgadas y 128GB de almacenamiento', 300.00, 15),
    ('Mouse Gaming', 'Mouse gaming con 16,000 DPI y retroiluminación RGB', 45.00, 35),
    ('Teclado Mecánico', 'Teclado mecánico con retroiluminación RGB y switches azules', 70.00, 30),
    ('Monitor Curvo', 'Monitor curvo de 27 pulgadas, resolución 1440p', 250.00, 10),
    ('Router Wi-Fi', 'Router Wi-Fi de doble banda con alcance extendido', 60.00, 50),
    ('SSD 500GB', 'Disco SSD de 500GB con interfaz SATA III', 90.00, 25),
    ('Estación de Carga', 'Estación de carga para múltiples dispositivos USB', 35.00, 40),
    ('Batería Externa', 'Batería externa de 10,000 mAh para dispositivos móviles', 25.00, 60),
    ('Cargador Inalámbrico', 'Cargador inalámbrico rápido para smartphones', 20.00, 50),
    ('Gafas VR', 'Gafas de realidad virtual con soporte para móviles', 120.00, 15),
    ('Joystick', 'Joystick inalámbrico para juegos con vibración', 50.00, 30),
    ('Pantalla Táctil', 'Pantalla táctil de 7 pulgadas para Raspberry Pi', 45.00, 20),
    ('Teclado Bluetooth', 'Teclado Bluetooth compacto para dispositivos móviles', 35.00, 25),
    ('Hub USB', 'Hub USB 3.0 de 4 puertos para expansión de puertos', 15.00, 55)";
    

if ($conn->query($sql) === TRUE) {
    echo "Datos insertados en 'productos' exitosamente.<br>";
} else {
    echo "Error al insertar datos en 'productos': " . $conn->error . "<br>";
}

// Insertar datos en la tabla 'logs'
$sql = "INSERT INTO logs (id_usuario, accion, id_producto) VALUES
    (1, 'Agregar', 1),
    (1, 'Agregar', 2),
    (2, 'Consultar', NULL),
    (1, 'Modificar', 3)";

if ($conn->query($sql) === TRUE) {
    echo "Datos insertados en 'logs' exitosamente.<br>";
} else {
    echo "Error al insertar datos en 'logs': " . $conn->error . "<br>";
}

// Cerrar conexión
$conn->close();
?>
