<?php
$servername = "localhost";
$db_username = "root"; 
$db_password = ""; 
$database = "sadp"; 

// Crear conexión
$conn = new mysqli($servername, $db_username, $db_password, $database);

// Verificar conexión
if ($conn->connect_error) {
    die("Conexión fallida: " . $conn->connect_error);
}