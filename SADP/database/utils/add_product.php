<?php

// Configuración de conexión a la base de datos
$host = 'localhost';
$dbname = 'sadp';
$username = 'root';
$password = '';

try {
    $db = new PDO("mysql:host=$host;dbname=$dbname;charset=utf8", $username, $password);
    $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
} catch (PDOException $e) {
    die('Error de conexión: ' . $e->getMessage());
}

// Configurar cabecera para JSON
header('Content-Type: application/json');

// Obtener los datos enviados desde la solicitud AJAX
$nombre = isset($_POST['nombre']) ? $_POST['nombre'] : null;
$descripcion = isset($_POST['descripcion']) ? $_POST['descripcion'] : null;
$precio = isset($_POST['precio']) ? (float)$_POST['precio'] : null;
$stock = isset($_POST['stock']) ? (int)$_POST['stock'] : null;

if ($nombre === null || $descripcion === null || $precio === null || $stock === null) {
    echo json_encode(['error' => 'Todos los campos son obligatorios.']);
    exit();
}

try {
    // Preparar la consulta de inserción
    $query = $db->prepare("
        INSERT INTO productos (nombre_producto, descripcion, precio, stock)
        VALUES (:nombre, :descripcion, :precio, :stock)
    ");
    $query->bindParam(':nombre', $nombre, PDO::PARAM_STR);
    $query->bindParam(':descripcion', $descripcion, PDO::PARAM_STR);
    $query->bindParam(':precio', $precio, PDO::PARAM_STR);
    $query->bindParam(':stock', $stock, PDO::PARAM_INT);
