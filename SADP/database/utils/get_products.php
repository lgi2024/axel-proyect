<?php

// Configuración de conexión a la base de datos
$host = 'localhost';
$dbname = 'sadp';
$username = 'root';
$password = '';


ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);



try {
    $db = new PDO("mysql:host=$host;dbname=$dbname;charset=utf8", $username, $password);
    $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
} catch (PDOException $e) {
    die('Error de conexión: ' . $e->getMessage());
}


// Configurar cabecera para JSON
header('Content-Type: application/json');

try {
    // Verificar si los parámetros de paginación están configurados
    $page = isset($_GET['page']) ? (int)$_GET['page'] : 1;
    $limit = 10; // Número de productos por página
    $offset = ($page - 1) * $limit;

    // Consultar los productos con límites y desplazamiento
    $query = $db->prepare("SELECT id_producto, nombre_producto, descripcion, precio, stock FROM productos LIMIT :limit OFFSET :offset");
    $query->bindParam(':limit', $limit, PDO::PARAM_INT);
    $query->bindParam(':offset', $offset, PDO::PARAM_INT);
    $query->execute();

    $products = $query->fetchAll(PDO::FETCH_ASSOC);

    // Obtener el total de productos para calcular páginas
    $totalQuery = $db->query("SELECT COUNT(*) AS total FROM productos");
    $totalProducts = $totalQuery->fetch(PDO::FETCH_ASSOC)['total'];
    $totalPages = ceil($totalProducts / $limit);

    // Devolver los datos como JSON
    echo json_encode([
        'products' => $products,
        'totalPages' => $totalPages
    ]);
} catch (Exception $e) {
    echo json_encode([
        'error' => $e->getMessage()
    ]);
}
