<?php
include 'database/connection/db_connection.php';

// Configurar el encabezado antes de cualquier salida
header('Content-Type: application/json');

// Obtener la página actual desde los parámetros GET (por defecto es la página 1)
$page = isset($_GET['page']) && is_numeric($_GET['page']) ? (int)$_GET['page'] : 1;
$limit = 10; // Número de productos por página
$offset = ($page - 1) * $limit;

// Consultar el número total de productos
$totalQuery = $conn->query("SELECT COUNT(*) as total FROM productos WHERE stock > 0");
$totalProducts = $totalQuery->fetch_assoc()['total'];
$totalPages = ceil($totalProducts / $limit); // Calcular el número total de páginas

// Consultar los productos para la página actual
$query = $conn->prepare("SELECT id_producto, nombre_producto, descripcion, precio, stock FROM productos WHERE stock > 0 LIMIT ? OFFSET ?");
$query->bind_param("ii", $limit, $offset);
$query->execute();
$result = $query->get_result();

// Generar el HTML de los productos
$productsHtml = '<table border="1" cellpadding="10" cellspacing="0" style="width:100%">';
$productsHtml .= '<thead>
    <tr>
        <th>Nombre</th>
        <th>Descripción</th>
        <th>Precio</th>
        <th>Stock</th>
    </tr>
</thead><tbody>';

while ($row = $result->fetch_assoc()) {
    $productsHtml .= '<tr>
        <td>' . htmlspecialchars($row['nombre_producto']) . '</td>
        <td>' . htmlspecialchars($row['descripcion']) . '</td>
        <td>$' . htmlspecialchars($row['precio']) . '</td>
        <td>' . htmlspecialchars($row['stock']) . '</td>
    </tr>';
}
$productsHtml .= '</tbody></table>';

// Generar el HTML para los botones de paginación
$paginationHtml = '<div>';
for ($i = 1; $i <= $totalPages; $i++) {
    $paginationHtml .= '<button onclick="loadProducts(' . $i . ')">' . $i . '</button> ';
}
$paginationHtml .= '</div>';

// Enviar la respuesta en formato JSON
echo json_encode([
    'productsHtml' => $productsHtml,
    'paginationHtml' => $paginationHtml
]);
?>
