<?php
session_start();
include '../database/connection/db_connection.php';

if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    $input = $_POST['username'] ?? '';  // El campo puede ser nombre de usuario o correo
    $password = $_POST['password'] ?? '';  // Contraseña proporcionada por el usuario

    if (!empty($input) && !empty($password)) {
        // Determinar si el input es un correo electrónico o un nombre de usuario
        if (filter_var($input, FILTER_VALIDATE_EMAIL)) {
            // Si es un correo, buscamos por el campo email
            $stmt = $conn->prepare("SELECT id_usuario, nombre, contraseña, rol FROM usuarios WHERE email = ?");
        } else {
            // Si no es un correo, buscamos por el campo nombre (usuario)
            $stmt = $conn->prepare("SELECT id_usuario, nombre, contraseña, rol FROM usuarios WHERE nombre = ?");
        }

        // Vinculamos el parámetro y ejecutamos la consulta
        $stmt->bind_param("s", $input);
        $stmt->execute();
        $result = $stmt->get_result();

        if ($result->num_rows > 0) {
            $user = $result->fetch_assoc();

            // Verificar la contraseña utilizando password_verify() en lugar de comparación directa
            if (password_verify($password, $user['contraseña'])) {
                $_SESSION['user_id'] = $user['id_usuario'];
                $_SESSION['user_name'] = $user['nombre'];
                $_SESSION['user_role'] = $user['rol'];

                // Responder con éxito y el rol del usuario
                echo json_encode(['success' => true, 'role' => $user['rol']]);
            } else {
                echo json_encode(['success' => false, 'message' => 'Contraseña incorrecta.']);
            }
        } else {
            echo json_encode(['success' => false, 'message' => 'Usuario no encontrado.']);
        }
    } else {
        echo json_encode(['success' => false, 'message' => 'Todos los campos son obligatorios.']);
    }
} else {
    http_response_code(405);
    echo json_encode(['success' => false, 'message' => 'Método no permitido.']);
}
?>
