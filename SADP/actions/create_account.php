<?php
session_start(); // Asegúrate de iniciar la sesión al comienzo
include '../database/connection/db_connection.php';

if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    $name = $_POST['username'] ?? '';
    $email = $_POST['email'] ?? '';
    $password = $_POST['password'] ?? '';
    $confirm_password = $_POST['confirm-password'] ?? '';

    if ($password !== $confirm_password) {
        echo json_encode(['success' => false, 'message' => 'Las contraseñas no coinciden.']);
        exit;
    }

    if (!empty($name) && !empty($email) && !empty($password)) {
        $hashed_password = password_hash($password, PASSWORD_BCRYPT);

        $stmt = $conn->prepare("INSERT INTO usuarios (nombre, email, contraseña, rol) VALUES (?, ?, ?, 'cliente')");
        $stmt->bind_param("sss", $name, $email, $hashed_password);

        if ($stmt->execute()) {
            // Establecer variables de sesión después de crear la cuenta exitosamente
            $_SESSION['user_id'] = $conn->insert_id; // Último ID insertado
            $_SESSION['user_name'] = $name;
            $_SESSION['user_role'] = 'cliente';

            echo json_encode(['success' => true, 'message' => 'Cuenta creada exitosamente.']);
        } else {
            echo json_encode(['success' => false, 'message' => 'Error al crear la cuenta.']);
        }
    } else {
        echo json_encode(['success' => false, 'message' => 'Todos los campos son obligatorios.']);
    }
} else {
    http_response_code(405);
    echo json_encode(['success' => false, 'message' => 'Método no permitido.']);
}
?>
